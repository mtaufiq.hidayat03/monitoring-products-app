﻿# Project Monitoring Products

All you need before run :

```bash
1. Install git: https://git-scm.com/

2. Install postgresql version 12: https://www.postgresql.org/download/

3. Install apache web server: https://httpd.apache.org/download.cgi

4. Install PHP version 7.4: https://www.php.net/downloads.php

5. Install Node.js: https://nodejs.org/en/download/

6. Install composer: https://getcomposer.org/download/

7. Install all php depencies for laravel : OpenSSL PHP Extension, PDO PHP Extension, Mbstring PHP Extension, Tokenizer PHP Extension, XML PHP Extension

8. Git clone from this url: https://gitlab.com/mtaufiq.hidayat03/master-of-products-app.git

9. Database using postgresql and create database with name : monitoring_products

10. Export sql in folder database with name postgres_dump.sql

11. Create .env file: copy all entries of env.example and paste into .env file

```

All you need is to run these commands:
```bash
1. Open folder project in terminal or command prompt.

2. Run command to install composer in project : composer install 

3. Run command to install node package manager (npm) in project : npm install

3. Generate key : php artisan key:generate

4. *) Run : php artisan serve 

   *) Run with custom port : php artisan serve --port=8031

```

Demo App : [Monitoring Products](http://localhost)

Demo App With Custom Port: [Monitoring Products](http://localhost:8031)

Question number 1 :

Relation among of tables :
There are 5 tables with this project :
1. master_products_brands : for data master of product brand (only for complement - not including in question)
2. measures : for data master of measure (only for complement - not including in question)
3. master_products : for data product level 1. 
4. master_categories : for data product level 2. There is column "id" in table master_products which has relationship one to many with column "pid" in table master_products. 
5. master_product_details for data product level 3. There is column "id" in table master_products which has relationship one to many with column "product_id" in table master_product_details. There is column "id" in table master_categories which has relationship one to many with column "category_id" in table master_product_details. There is column "id" in table measures which has relationship one to many with column "measure_id" in table master_product_details. There is column "id". There is column "id" in table master_products_brands which has relationship one to many with column "brand_id" in table master_product_details.

Question Number 2 :

